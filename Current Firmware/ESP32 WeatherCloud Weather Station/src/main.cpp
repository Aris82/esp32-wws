/*!
*    @file main.cpp
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This C++ file is the main runtime of the program, calling method from all the header files.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    2) Power.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    3) Network.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    4) Packet.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    4) SerialInterface.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    6) Temperature.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    7) HumidityPressure.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    8) Rain.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    9) Solar.h header file (a part of the ESP32 Weathercloud Weather Station project)
*    10) Wind.h header file (a part of the ESP32 Weathercloud Weather Station project)
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

// Utility classes
#include<Utility/Utils.h>
#include<Utility/Network.h>
#include<Utility/Power.h>
#include<Utility/Packet.h>
#include<Utility/SerialInterface.h>

// Sensor classes
#include<Sensors/Temperature.h>
#include<Sensors/PressureHumidity.h>
#include<Sensors/Wind.h>
#include<Sensors/Solar.h>
#include<Sensors/Rain.h> 


// Occurs during the MCU initialization
void setup ()
{
    SerialInterface::begin();

    Network::begin();
    Network::connect();

    Temperature::begin();
    PressureHumidity::begin();
    Rain::begin();
    Solar::begin();
}

// The main program runtime
void loop () 
{
    Packet packet = Packet(
        Temperature::measure_air(),
        Temperature::measure_ground(),
        Temperature::calculate_wind_chill(Wind::measure_wind_speed()),
        Temperature::calculate_dew_point(PressureHumidity::measure_humidity()),
        Temperature::calculate_heat_index(PressureHumidity::measure_humidity()),
        PressureHumidity::measure_humidity(),
        PressureHumidity::measure_pressure(),
        Wind::measure_wind_speed(),
        Wind::measure_wind_direction(),
        Rain::measure_rainfall(),
        Rain::measure_rain_rate(),
        Solar::measure_uv_index(),
        Solar::measure_solar_radiation()        
    );

    SerialInterface::print_packet(packet);
    Network::send(packet);

    Power::sleep(600);
}