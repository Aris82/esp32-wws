/*!
*    @file Packet.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to save and return the measured sensor variables that are later
*    either printed to the console or sent to the Weathercloud API.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project)
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Project libraries
#include<Utility/Utils.h>


// Weathercloud data packet definition
class Packet {
public:
    // Packet variables
    float temperature;
    float temperature_inside;
    float wind_chill;
    float dew_point;
    float heat_index;
    float humidity;
    float wind_speed;
    float wind_direction;
    float pressure;
    float rainfall;
    float rain_rate;
    float uv_index;
    float solar_radiation;

    // Packet constructor
    Packet(float temp, float tempin, float wchill, float dew, float heat, float hum, float pres,float wspd, int wdir, float rain, float rainrate, float uv, float solar) 
    {
        temperature = temp;
        temperature_inside = tempin;
        wind_chill = wchill;
        dew_point = dew;
        heat_index = heat;
        humidity = hum;
        pressure = pres;
        wind_speed = wspd;
        wind_direction = wdir;
        rainfall = rain;
        rain_rate = rainrate;
        uv_index = uv;
        solar_radiation = solar;
    }

    // Compressess the data packet before it is sent 
    void compress()
    {
        temperature        = float_to_int_decimal(temperature);
        temperature_inside = float_to_int_decimal(temperature_inside);
        wind_chill         = float_to_int_decimal(wind_direction);
        dew_point          = float_to_int_decimal(dew_point);
        heat_index         = float_to_int_decimal(heat_index);
        humidity           = float_to_int(humidity);
        pressure           = float_to_int_decimal(pressure);
        wind_speed         = float_to_int_decimal(temperature_inside);
        wind_direction     = float_to_int(wind_direction);
        rainfall           = float_to_int_decimal(rainfall);
        rain_rate          = float_to_int_decimal(rain_rate);
        uv_index           = float_to_int_decimal(uv_index);
        solar_radiation    = float_to_int_decimal(solar_radiation);
    }

private:
    // Functions for data compression
    int float_to_int (float input)
    {
        return int(input);
    }
    int float_to_int_decimal (float input)
    {
        return int(input) * 10;
    }
};