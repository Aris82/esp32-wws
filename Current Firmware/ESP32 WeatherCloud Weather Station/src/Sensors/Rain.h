/*!
*    @file Rain.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to measure the analog values from the rain sensor and interpret
*    them as rain rate or rainfall values.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project) 
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Time library
#include <time.h>


// Used to measure the analog values from the rain sensor and interpret them
class Rain {
public:
    // Add a interrupt trigger for the rain gauge
    static void begin()
    {
        attachInterrupt(RAIN_INPUT, gauge_click_interrupt, FALLING);
    }

    // Rain gauge interrupt handler, triggers when the rain gauge clicks
    static void gauge_click_interrupt()
    {
        rainfall += BUCKET_VOLUME;
        rainrate += BUCKET_VOLUME;
    }

    // Returns the measured rainfall
    static float measure_rainfall()
    {
        // If hour or day changed, reset the rainfall/rainrate counters
        if(Network::timeinfo.tm_hour != last_read.tm_hour) rainrate = 0;
        if(Network::timeinfo.tm_mday != last_read.tm_mday) rainfall = 0;
        last_read = Network::timeinfo;
    
        return rainfall;
    }

    // Returns the measured rainrate
    static float measure_rain_rate()
    {
        // If hour or day changed, reset the rainfall/rainrate counters
        if(Network::timeinfo.tm_hour != last_read.tm_hour) rainrate = 0;
        if(Network::timeinfo.tm_mday != last_read.tm_mday) rainfall = 0;
        last_read = Network::timeinfo;
    
        return rainrate;
    }

private:
    // Variables to hold rainfall and rainrate
    RTC_DATA_ATTR static float rainfall, rainrate;

    // Variable to store the last read time
    RTC_DATA_ATTR static tm last_read;

    // Rainfall sensor input pin
    static const int RAIN_INPUT = 1;

    // Tipping bucket volume (depends on the rain gauge)
    static const float BUCKET_VOLUME;
};

float Rain::rainfall;
float Rain::rainrate;
tm Rain::last_read;
const float Rain::BUCKET_VOLUME = 0.28;