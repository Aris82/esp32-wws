/*!
*    @file Utils.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header contains the generic functions and variables that should be accesible from
*    anywhere in the codebase. This header should be included in every other embedded header in the
*    ESP32 Weathercloud Weather Station project.
*
*    @section Dependencies
*
*    1) The Arduino framework.
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Include the Arduino framework
#include <Arduino.h>