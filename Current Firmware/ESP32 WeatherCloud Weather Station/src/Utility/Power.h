/*!
*    @file Power.h 
*
*    @section Introduction
*
*    This file is a part of the ESP32 Weathercloud Weather Station project. This program is used to run
*    all the systems of the weather station such as power, network and all the sensors.
*
*    This specific header is used to put the processor of the weather station into sleep and wake it
*    up if needed in order to save power.
*
*    @section Dependencies
*
*    1) Utils.h header file (a part of the ESP32 Weathercloud Weather Station project) 
*
*    @author Author
*
*    This program is developed by Jakub Nagy. See <https://linktr.ee/Jakub_Nagy>. 
*
*    @section Documentation
*
*    The repository with information about this program is located at <https://www.instructables.com/ESP32-Weathercloud-Weather-Station/>.
*    The Instructable project, which uses this program is located at <https://gitlab.com/instructables-jakub-nagy/esp32-wws-software>.
*
*/

#pragma once

// Project libraries
#include<Utility/Utils.h>


// Used to put the processor of the weather station into sleep to save power
class Power {
public:
    // Put the processor to sleep for the inputted time
    static void sleep(int time)
    {
        SerialInterface::println("System entering hibernation mode.");
        
        esp_sleep_enable_timer_wakeup(time * 1000000);
        delay(100);
        Serial.flush(); 
        esp_deep_sleep_start();
    }
};